# WorkshopGr5

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.3.

## Procédure d'installation

Toutes les instructions qui vont suivre doivent être exécutées dans le répertoire du projet.

### Installation des dépendances

```npm install```

### Lancement du serveur

```npm start```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

