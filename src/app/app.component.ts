import { Component, HostListener, OnInit } from '@angular/core';
import { OnlineStatusService, OnlineStatusType } from 'ngx-online-status';
import { MessageService, PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [MessageService]
})
export class AppComponent implements OnInit {
  public isOnline: boolean = true;

  constructor(private messageService: MessageService, private primeNGConfig: PrimeNGConfig, private onlineStatusService: OnlineStatusService) { }

  ngOnInit(): void {
    this.primeNGConfig.ripple = true;

    this.onlineStatusService.status.subscribe((status: OnlineStatusType) => {
      if (status === OnlineStatusType.OFFLINE) {
        this.messageService.add({ severity: 'info', summary: 'Network', detail: 'Offline' });
      } else {
        this.messageService.add({ severity: 'info', summary: 'Network', detail: 'Online' });
      }
    });
  }
}
