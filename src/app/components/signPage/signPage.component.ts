import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'src/app/shared/services/navbar.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from "../../shared/services/auth.service";
import { EncryptDecryptService } from "../../shared/services/encrypt-decrypt.service";
import { MessageService } from "primeng/api";

@Component({
  selector: 'app-dashboard',
  templateUrl: './signPage.component.html',
  styleUrls: ['./signPage.component.scss'],
  providers: [MessageService]
})
export class SignPageComponent implements OnInit {
  public data!: any;
  public isSign: boolean = false;

  constructor(
    private messageService: MessageService,
    private navbarService: NavbarService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private encryptDecryptService: EncryptDecryptService
  ) {
    this.navbarService.isActive = false;
  }

  ngOnInit(): void {
    const data = this.route.snapshot.paramMap.get('data');
    if (data !== null) {
      const decryptDataURI = decodeURIComponent(data);
      const decryptedText = this.encryptDecryptService.decrypt(decryptDataURI);
      this.data = JSON.parse(decryptedText);
    }
  }

  validateSignature(): void {
    if (this.data.qrcode) {
      const user = this.data.user.attributes.user.data;
      //TODO: update la signature
      this.messageService.add({
        severity: 'success',
        summary: user.attributes.firstname + ' ' + user.attributes.lastname + ' a signé(e)'
      });
    } else {
      const user = this.data.user.attributes.user.data;
      //TODO: update la signature
      this.messageService.add({
        severity: 'success',
        summary: user.attributes.firstname + ' ' + user.attributes.lastname + ' a signé(e)'
      });
    }
    this.isSign = true;

  }

}
