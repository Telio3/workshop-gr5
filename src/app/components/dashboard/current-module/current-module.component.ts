import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Module } from 'src/app/shared/interfaces/module.interface';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Observable, take } from 'rxjs';
import { ModulesService } from 'src/app/shared/services/modules.service';

@Component({
  selector: 'app-current-module',
  templateUrl: './current-module.component.html',
  styleUrls: ['./current-module.component.scss']
})
export class CurrentModuleComponent implements OnInit {
  private modules$: Observable<Module[]> = this.modulesService.selectorGetModules();

  public currentModule!: Module;

  public mySignature: any;

  public nbLearners: number = 0;
  public nbNoSign: number = 0;
  public nbSign: number = 0;

  constructor(public authService: AuthService, private modulesService: ModulesService) { }

  ngOnInit(): void {
    this.modules$.subscribe({
      next: (modules) => {
        modules.map(module => {
          if (this.isNow(module.attributes.dateStart, module.attributes.dateEnd)) {
            this.currentModule = module;

            this.authService.isSpeaker$.subscribe(isSpeaker => {
              if (isSpeaker) {
                const signatures = [...this.currentModule.attributes.signatures.data];

                const indexSpeaker = signatures.findIndex(signature => signature.attributes.user.data.attributes.role.data.attributes.name === 'speaker');
                signatures.splice(indexSpeaker, 1);

                if (signatures.length) {
                  this.nbLearners = signatures.length;
                  this.nbNoSign = signatures.filter((signature: any) => signature.attributes.status === 'Attendu').length;
                  this.nbSign = signatures.filter((signature: any) => signature.attributes.status === 'Signé').length;
                }
              } else {
                const signatures = this.currentModule.attributes.signatures.data;

                this.authService.user$.pipe(take(1)).subscribe(user => {
                  if (user) {
                    const currentUser = { ...user };
                    this.mySignature = signatures.find((signature: any) => signature.attributes.user.data.id === currentUser.id);
                  }
                });
              }
            });
            return;
          }
        });
      }
    });
  }

  isNow(start: string, end: string): boolean {
    const now = moment();
    const startDate = moment(start);
    const endDate = moment(end);

    return now >= startDate && now <= endDate;
  }
}
