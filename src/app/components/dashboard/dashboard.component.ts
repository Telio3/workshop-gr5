import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ModulesService } from 'src/app/shared/services/modules.service';
import { NavbarService } from 'src/app/shared/services/navbar.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  constructor(public authService: AuthService, private navbarService: NavbarService, private modulesService: ModulesService) { this.navbarService.isActive = true; }

  ngOnInit(): void {
    this.modulesService.getModules().subscribe();
  }
}
