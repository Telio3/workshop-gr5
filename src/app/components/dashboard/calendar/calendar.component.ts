import { AfterViewInit, Component, HostListener, OnInit, ViewChild } from '@angular/core';
import {
  CalendarApi,
  CalendarOptions,
  EventClickArg,
  EventSourceInput,
  FullCalendarComponent
} from '@fullcalendar/angular';
import frLocale from '@fullcalendar/core/locales/fr';
import { Router } from "@angular/router";
import { ModulesService } from 'src/app/shared/services/modules.service';
import { Observable } from 'rxjs';
import { Module } from 'src/app/shared/interfaces/module.interface';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, AfterViewInit {
  public isOnlyList: boolean = true;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    if (event.target.innerWidth < 1100) {
      this.initOnlyList();
    } else {
      this.initAgenda();
    }
  }

  public modules$: Observable<Module[]> = this.modulesService.selectorGetModules();

  @ViewChild('calendar', { static: true }) calendar!: FullCalendarComponent;

  private calendarApi!: CalendarApi;

  public stateOptions = [{ label: 'Agenda', value: 'dayGridMonth' }, { label: 'Liste', value: 'listWeek' }];
  public defaultOption: string = 'dayGridMonth';

  public calendarOptions: CalendarOptions = {
    locales: [frLocale],
    buttonText: {
      listMonth: 'Mois',
      listYear: 'Année',
      listWeek: 'Semaine',
      listDay: 'Jour'
    },
    eventClick: ((info: EventClickArg): void => {
      this.router.navigate(['modules', info.event.id]);
    }),
  };

  constructor(private router: Router, private modulesService: ModulesService) { }

  ngOnInit(): void {
    this.modules$.subscribe({
      next: (modules) => {
        const events: EventSourceInput = [];

        modules.map(module => {
          events.push({
            id: String(module.id),
            title: module.attributes.title,
            start: module.attributes.dateStart,
            end: module.attributes.dateEnd,
          });
        });

        this.calendarOptions.events = events;
      }
    });
  }

  ngAfterViewInit(): void {
    this.calendarApi = this.calendar.getApi();

    if (window.innerWidth < 1100) {
      this.initOnlyList();
    } else {
      this.initAgenda();
    }
  }

  initOnlyList(): void {
    this.isOnlyList = true;

    this.calendarOptions.headerToolbar = {
      left: 'prev,next today',
      right: 'title',
    };

    this.calendarApi.changeView('listWeek');
  }

  initAgenda(): void {
    this.isOnlyList = false;

    this.onChangeState({ value: 'timeGridWeek' });
  }

  onChangeState(event: any): void {
    if (event.value === 'listWeek') {
      this.calendarOptions.headerToolbar = {
        left: 'prev,next today',
        center: 'title',
        right: 'listYear,listMonth,listWeek,listDay'
      };

      this.calendarApi.changeView('listWeek');
    } else {
      this.calendarOptions.headerToolbar = {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      };

      this.calendarApi.changeView('timeGridWeek');
    }
  }

  onClickModule(module: any): void {
    this.calendarApi.gotoDate(module.attributes.dateStart);
  }
}
