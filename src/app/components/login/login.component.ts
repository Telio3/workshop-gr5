import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NavbarService } from 'src/app/shared/services/navbar.service';
import { EncryptDecryptService } from "../../shared/services/encrypt-decrypt.service";
import { ModulesService } from "../../shared/services/modules.service";
import { Observable } from "rxjs";
import { Module } from "../../shared/interfaces/module.interface";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public formGroup = this.fb.group({
    identifier: ['', Validators.email],
    password: ['', Validators.required]
  });

  public error!: string;

  public data!: any;

  private module$!: Observable<Module | undefined>;
  public module!: Module;

  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private navbarService: NavbarService,
    private route: ActivatedRoute,
    private encryptDecryptService: EncryptDecryptService,
    private modulesService: ModulesService
  ) {
    this.navbarService.isActive = false;
  }

  ngOnInit() {
    this.data = this.route.snapshot.paramMap.get('data');
    if (this.data) {
      const decryptDataURI = decodeURIComponent(this.data);
      const decryptedDataText = this.encryptDecryptService.decrypt(decryptDataURI);
      const decryptedData = JSON.parse(decryptedDataText);
      if (decryptedData.moduleId) {
        this.modulesService.getModule(decryptedData.moduleId).subscribe();
        this.module$ = this.modulesService.selectorGetModule(decryptedData.moduleId);

        this.module$.subscribe(module => {
          if (module) {
            this.module = module;
          }
        });
      }
    }
  }

  onSubmit(): void {
    if (this.formGroup.valid) {
      this.authService.login(this.formGroup.getRawValue()).subscribe(
        (response) => {
          if (this.data) {
            const decryptDataURI = decodeURIComponent(this.data);
            const decryptedDataText = this.encryptDecryptService.decrypt(decryptDataURI);
            const decryptedData = JSON.parse(decryptedDataText);

            if (decryptedData) {
              if (decryptedData.qrcode) {
                let signature = null;
                const signatures = this.module.attributes.signatures.data;

                for (let i = 0; i < signatures.length; i++) {
                  if (response.user.id === signatures[i].attributes.user.data.id) {
                    signature = signatures[i];
                  }
                }

                if (signature) {
                  let newData = { qrcode: decryptedData.qrcode, module: this.module, user: signature };

                  let encryptedNewData = this.encryptDecryptService.encrypt(JSON.stringify(newData));
                  let encryptedNewDataURI = encodeURIComponent(encryptedNewData);
                  this.router.navigate(['sign', encryptedNewDataURI]);
                } else {
                  this.error = `Vous ne pouvez pas signer pour un module auquel vous ne participez pas !`;
                }
              } else {
                if (response.user.id === decryptedData.user.attributes.user.data.id) {
                  this.router.navigate(['sign', this.data]);
                } else {
                  this.error = `Vous ne pouvez pas signer à la place d'un autre utilisateur !`;
                }
              }
            } else this.router.navigateByUrl('/');
          } else {
            this.router.navigateByUrl('/');
          }
        },
        () => {
          this.error = 'Mauvais mot de passe / email';
        }
      );
    }
  }
}
