import { Component, OnInit } from '@angular/core';
import { User } from "../../shared/interfaces/user.interface";
import { ConfirmationService, MessageService } from "primeng/api";
import { NavbarService } from 'src/app/shared/services/navbar.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from "../../shared/services/auth.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import * as moment from 'moment';
import { environment } from "../../../environments/environment";
import { Module } from 'src/app/shared/interfaces/module.interface';
import { Observable } from 'rxjs';
import { EncryptDecryptService } from "../../shared/services/encrypt-decrypt.service";
import { ModulesService } from 'src/app/shared/services/modules.service';
import { OnlineStatusService, OnlineStatusType } from 'ngx-online-status';

@Component({
  selector: 'app-dashboard',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.scss'],
  providers: [ConfirmationService, MessageService]
})
export class ModuleComponent implements OnInit {
  public isOnline: boolean = true;

  private id!: number;

  private module$!: Observable<Module | undefined>;
  public module!: Module;

  public formGroup!: FormGroup;

  public currentUser!: User | null;

  public speakers: any[] = [];
  public selectedUsers = [];

  public showCloseModal: boolean = false;

  public showCommentModal: boolean = false;
  public headerCommentModal: string = '';
  public idSignatureComment: number = 0;

  public showEditUser: boolean = false;

  public showSignModal: boolean = false;
  public userSigning: any | null;

  public showQRcode: boolean = false;
  public qrdata: string = '';

  constructor(
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private navbarService: NavbarService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private fb: FormBuilder,
    private encryptDecryptService: EncryptDecryptService,
    private modulesService: ModulesService,
    private onlineStatusService: OnlineStatusService
  ) {
    this.navbarService.isActive = true;
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));

    this.modulesService.getModule(this.id).subscribe();
    this.module$ = this.modulesService.selectorGetModule(this.id);

    this.module$.subscribe(module => {
      if (module) {
        this.module = module;

        let data = { qrcode: true, moduleId: module.id };
        let encryptedData = this.encryptDecryptService.encrypt(JSON.stringify(data));
        let encryptedDataURI = encodeURIComponent(encryptedData);
        this.qrdata = `${environment.urlApp}login/${encryptedDataURI}`;

        this.speakers = [];

        this.module.attributes.signatures.data.map(signature => {
          if (signature.attributes.user.data.attributes.role.data.attributes.name === 'speaker') {
            this.speakers.push(signature?.attributes.user.data);
          }
        });
      }
    });

    this.authService.user$.subscribe(user => {
      this.currentUser = user;
    });

    this.isOnline = (this.onlineStatusService.getStatus() === OnlineStatusType.ONLINE);

    this.onlineStatusService.status.subscribe((status: OnlineStatusType) => {
      this.isOnline = (status === OnlineStatusType.ONLINE);
    });
  }

  onRowSelect(signature: any): void {
    const user = signature.attributes.user.data;
    this.formGroup = this.fb.group({
      id: [user.id],
      username: [user.attributes.username, Validators.required],
      firstname: [user.attributes.firstname, Validators.required],
      lastname: [user.attributes.lastname, Validators.required],
    });
    this.showEditUser = true;
  }

  submitUpdateModal(): void {
    this.modulesService.updateUser(this.formGroup.getRawValue());
    this.closeUpdateModal();
  }

  closeUpdateModal(): void {
    this.showEditUser = false;
  }

  openModule(): void {
    this.confirmationService.confirm({
      message: 'Êtes-vous sûr de vouloir démarrer le module ?',
      header: 'Démarrer le module',
      acceptButtonStyleClass: 'p-button-success',
      rejectButtonStyleClass: 'p-button-danger',
      acceptLabel: 'Oui',
      rejectLabel: 'Non',
      accept: () => {
        this.modulesService.openModule(this.module.id);
        this.messageService.add({ severity: 'success', summary: 'Module démarré' });
      }
    });
  }

  openCloseModule(): void {
    this.confirmationService.confirm({
      message: 'Êtes-vous sûr de vouloir clôturer le module ?',
      header: 'Clôturer le module',
      acceptButtonStyleClass: 'p-button-success',
      rejectButtonStyleClass: 'p-button-danger',
      acceptLabel: 'Oui',
      rejectLabel: 'Non',
      accept: () => {
        this.showCloseModal = true;
      }
    });
  }

  closeModule(): void {
    this.showCloseModal = false;
    if (this.currentUser) {
      this.modulesService.closeModule(this.module.id, this.currentUser.id);
    }
    this.messageService.add({ severity: 'success', summary: 'Module cloturé', detail: 'Les mails de relance ont été envoyé' });
  }

  openSignature(signature: any): void {
    this.userSigning = signature;
    this.showSignModal = true;
  }

  validateSignature(): void {
    const user = this.userSigning.attributes.user.data;
    this.modulesService.leanerSignModule(this.module.id, this.userSigning.id);
    this.messageService.add({ severity: 'success', summary: user.attributes.firstname + ' ' + user.attributes.lastname + ' a signé(e)' });

    this.userSigning = null;
    this.showSignModal = false;
  }

  openSendMail(): void {
    this.confirmationService.confirm({
      message: 'Êtes-vous sûr de vouloir envoyer un mail de signature aux participants sélectionnés ?',
      header: 'Mail de signature',
      acceptButtonStyleClass: 'p-button-success',
      rejectButtonStyleClass: 'p-button-danger',
      acceptLabel: 'Oui',
      rejectLabel: 'Non',
      accept: () => {
        this.sendEmailToSelectedUsers();
      }
    });
  }

  openQRcode(): void {
    console.log(this.qrdata);
    this.showQRcode = true;
  }

  openCommentModule(signature?: any): void {
    if (signature) {
      this.idSignatureComment = signature.id;

      this.headerCommentModal = 'Commentaire pour ' + signature.attributes.user.data.attributes.firstname + ' ' + signature.attributes.user.data.attributes.lastname;

      this.formGroup = this.fb.group({
        comment: [signature.attributes.comment]
      });
    } else {
      this.idSignatureComment = 0;

      this.headerCommentModal = 'Commentaire du module';

      this.formGroup = this.fb.group({
        comment: [this.module.attributes.comment]
      });
    }

    this.showCommentModal = true;
  }

  onSubmitCommentModule(): void {
    if (this.formGroup.valid) {
      if (this.idSignatureComment === 0) {
        this.modulesService.updateCommentModule(this.module.id, this.formGroup.get('comment')?.value);
      } else {
        this.modulesService.updateCommentLearner(this.module.id, this.idSignatureComment, this.formGroup.get('comment')?.value);
      }
      this.showCommentModal = false;
    }
  }

  openAbsentUser(signature: any): void {
    this.confirmationService.confirm({
      message: signature.attributes.user.data.attributes.firstname + ' ' + signature.attributes.user.data.attributes.lastname + ' est absent ?',
      header: 'Absence',
      acceptButtonStyleClass: 'p-button-success',
      rejectButtonStyleClass: 'p-button-danger',
      acceptLabel: 'Oui',
      rejectLabel: 'Non',
      accept: () => {
        this.modulesService.updateAbsentLearner(this.module.id, signature.id);
      }
    });
  }

  sendEmailToSelectedUsers(): void {
    for (const user of this.selectedUsers) {
      let data = { qrcode: false, module: this.module, user: user };
      let encryptedData = this.encryptDecryptService.encrypt(JSON.stringify(data));
      let encryptedDataURI = encodeURIComponent(encryptedData);
      console.log(`${environment.urlApp}login/${encryptedDataURI}`);
    }
    this.selectedUsers = [];

    this.messageService.add({ severity: 'success', summary: 'Signature', detail: 'Mail envoyé' });
  }

  verifyIsOpenable(dateStart: string, dateEnd: string, moduleStatus: string): boolean {
    const now = moment();
    const realDateStart = moment(dateStart);
    const realDateEnd = moment(dateEnd);

    return realDateStart < now && now < realDateEnd && moduleStatus === 'Attente';
  }

  verifyIsClosable(dateEnd: string, moduleStatus: string): boolean {
    const now = moment();
    const realDateEnd = moment(dateEnd);

    return now > realDateEnd && moduleStatus === 'Ouvert';
  }
}
