import { Injectable } from '@angular/core';
import { Module, ModulesWrapper, ModuleWrapper } from '../interfaces/module.interface';
import { OnlineStatusService, OnlineStatusType } from 'ngx-online-status'
import { BehaviorSubject, map, Observable, take, tap } from 'rxjs';
import { StorageService } from './storage.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const STORAGE_MODULES = 'modules';
const STORAGE_MODULES_ID = 'modulesId';

@Injectable({
  providedIn: 'root'
})
export class ModulesService {
  private modules$ = new BehaviorSubject<Module[]>([]);

  constructor(private onlineStatus: OnlineStatusService, private storage: StorageService, private http: HttpClient) {
    this.loadModules();

    this.onlineStatus.status.subscribe((status: OnlineStatusType) => {
      if (status === OnlineStatusType.ONLINE) {
        const modulesId: any[] | null = this.storage.get(STORAGE_MODULES_ID);

        if (modulesId) {
          modulesId.forEach(id => {
            const module = [... this.modules$.value].find(m => m.id === id);
            if (module) {
              this.updateModule(id, { statut: module.attributes.statut, comment: module.attributes.comment });
            }
          });
        }

        this.storage.remove(STORAGE_MODULES_ID);
      }
    });
  }

  selectorGetModules(): Observable<Module[]> {
    return this.modules$.asObservable();
  }

  selectorGetModule(id: string | number): Observable<Module | undefined> {
    return this.modules$.pipe(
      map(modules => modules.find(m => m.id === id))
    );
  }

  getModules(): Observable<Module[]> {
    if (this.onlineStatus.getStatus() === OnlineStatusType.OFFLINE) {
      return this.selectorGetModules().pipe(take(1));
    }
    return this.http.get<ModulesWrapper>(environment.urlApi + 'modules').pipe(
      map((modules: ModulesWrapper) => {
        return modules.data;
      }),
      tap((modules: Module[]) => {
        this.setModules(modules);
      }),
    );
  }

  getModule(id: string | number): Observable<any> {
    if (this.onlineStatus.getStatus() === OnlineStatusType.OFFLINE) {
      return this.selectorGetModule(id).pipe(take(1));
    }
    return this.http.get<ModuleWrapper>(environment.urlApi + 'modules/' + id).pipe(
      tap((module: ModuleWrapper) => {
        const modules = [... this.modules$.value];
        const localModuleIndex: number = modules.findIndex(m => m.id === module.data.id);
        if (localModuleIndex !== -1) {
          modules.splice(localModuleIndex, 1);
        }
        modules.push(module.data);
        this.modules$.next(modules);
      }),
    );
  }

  private setModules(modules: Module[]): void {
    this.modules$.next(modules);
    this.storage.save(STORAGE_MODULES, modules);
  }

  private loadModules(): void {
    const modules = this.storage.get<Module[]>(STORAGE_MODULES);
    if (modules) {
      this.modules$.next(modules);
    }
  }

  updateSignature(idModule: string | number, idSignature: string | number, status: string): void {
    const modules = [... this.modules$.value];
    const localModuleIndex: number = modules.findIndex(m => m.id === idModule);

    const signature = modules[localModuleIndex].attributes.signatures.data.find((signature: any) => signature.id === idSignature);

    if (signature) {
      signature.attributes.status = status;
    }

    this.modules$.next(modules);
    this.storage.save(STORAGE_MODULES, modules);
  }

  updateUser(user: any): void {
    const modules = [... this.modules$.value];

    modules.forEach(module => {
      const signature = module.attributes.signatures.data.find((signature: any) => signature.attributes.user.data.id === user.id);

      if (signature) {
        const updateduser = signature.attributes.user.data.attributes;

        updateduser.username = user.username;
        updateduser.firstname = user.firstname;
        updateduser.lastname = user.lastname;
      }
    });

    this.modules$.next(modules);
    this.storage.save(STORAGE_MODULES, modules);
  }

  openModule(idModule: string | number): void {
    const modules = [... this.modules$.value];
    const localModuleIndex: number = modules.findIndex(m => m.id === idModule);

    modules[localModuleIndex].attributes.statut = 'Ouvert';

    this.modules$.next(modules);
    this.storage.save(STORAGE_MODULES, modules);

    this.updateModule(idModule, { statut: 'Ouvert' });
  }

  closeModule(idModule: string | number, idSpeaker: string | number): void {
    const modules = [... this.modules$.value];
    const localModuleIndex: number = modules.findIndex(m => m.id === idModule);

    modules[localModuleIndex].attributes.statut = 'Cloturé';

    modules[localModuleIndex].attributes.signatures.data.forEach(signature => {
      if (signature.attributes.user.data.id === idSpeaker) {
        signature.attributes.status = 'Signé';
      } else if (signature.attributes.status === 'Attendu') {
        signature.attributes.status = 'Absent';
      }
    });

    this.modules$.next(modules);
    this.storage.save(STORAGE_MODULES, modules);

    this.updateModule(idModule, { statut: 'Cloturé' });
  }

  leanerSignModule(idModule: string | number, idSignature: number): void {
    const modules = [... this.modules$.value];
    const localModuleIndex: number = modules.findIndex(m => m.id === idModule);

    const signature = modules[localModuleIndex].attributes.signatures.data.find((signature: any) => signature.id === idSignature);

    if (signature) {
      signature.attributes.status = 'Signé';
    }

    this.modules$.next(modules);
    this.storage.save(STORAGE_MODULES, modules);
  }

  updateCommentModule(idModule: string | number, comment: string): void {
    const modules = [... this.modules$.value];
    const localModuleIndex: number = modules.findIndex(m => m.id === idModule);

    modules[localModuleIndex].attributes.comment = comment;

    this.modules$.next(modules);
    this.storage.save(STORAGE_MODULES, modules);

    this.updateModule(idModule, { comment });
  }

  updateCommentLearner(idModule: string | number, idSignature: number, comment: string): void {
    const modules = [... this.modules$.value];
    const localModuleIndex: number = modules.findIndex(m => m.id === idModule);

    const signature = modules[localModuleIndex].attributes.signatures.data.find((signature: any) => signature.id === idSignature);

    if (signature) {
      signature.attributes.comment = comment;
    }

    this.modules$.next(modules);
    this.storage.save(STORAGE_MODULES, modules);
  }

  updateAbsentLearner(idModule: string | number, idSignature: number): void {
    const modules = [... this.modules$.value];
    const localModuleIndex: number = modules.findIndex(m => m.id === idModule);

    const signature = modules[localModuleIndex].attributes.signatures.data.find((signature: any) => signature.id === idSignature);

    if (signature) {
      signature.attributes.status = 'Absent';
    }

    this.modules$.next(modules);
    this.storage.save(STORAGE_MODULES, modules);
  }

  updateModule(idModule: string | number, body: object): void {
    if (this.onlineStatus.getStatus() === OnlineStatusType.ONLINE) {
      const bodyToSend = {
        data: { ...body }
      }

      this.http.put(environment.urlApi + 'modules/' + idModule, bodyToSend).subscribe();
    } else {
      const modulesId: any[] = this.storage.get(STORAGE_MODULES_ID) ?? [];
      modulesId.push(idModule);
      this.storage.save(STORAGE_MODULES_ID, modulesId);
    }
  }
}
