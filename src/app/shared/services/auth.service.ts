import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, catchError, Observable, of, ReplaySubject, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../interfaces/user.interface';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public isLoggedin$: ReplaySubject<boolean> = new ReplaySubject(1);
  public isSpeaker$: ReplaySubject<boolean> = new ReplaySubject(1);
  public user$: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(null);

  constructor(private http: HttpClient, private router: Router, private storage: StorageService) { }

  fetchCurrentUser(): Observable<User | null> {
    return this.http.get<User>(environment.urlApi + 'users/me').pipe(
      tap((user: User) => {
        this.user$.next(user);
        this.storage.save('user', user);
        this.isLoggedin$.next(true);
        this.isSpeaker$.next(user.role?.name === 'speaker');
      }),
      catchError(() => {
        if (localStorage.getItem('token')) {
          this.isLoggedin$.next(true);

          const user: User | null = this.storage.get('user');
          if (user) {
            this.user$.next(user);
            this.isSpeaker$.next(user.role?.name === 'speaker');
          }
        } else {
          this.logout();
        }
        return of(null);
      })
    );
  }

  login(credentials: { identifier: string; password: string; }): Observable<User> {
    return this.http.post(environment.urlApi + 'auth/local', credentials).pipe(
      tap((data: any) => {
        if (data.user) {
          this.isLoggedin$.next(true);
          localStorage.setItem('token', data.jwt);
        }
      })
    );
  }

  logout(): void {
    localStorage.removeItem('token');
    this.user$.next(null);
    this.storage.remove('user');
    this.isLoggedin$.next(false);
    this.router.navigateByUrl('/login');
  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }
}
