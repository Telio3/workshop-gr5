export interface User {
    signature: any
    id: number,
    firstname: string,
    lastname: string,
    email: string,
    modules: [
        {
            id: number,
            title: string,
            location: string,
            dateStart: Date,
            dateEnd: Date,
            statut: string
        }?
    ],
    role: {
        name: string,
    },
    user: {
      id: number
    }
}
