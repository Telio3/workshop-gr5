import { StrapiResponse } from "./strapi-response.interface";

export interface Module {
  id: number;
  attributes: {
    title: string;
    location: string;
    dateStart: string;
    dateEnd: string;
    statut: string;
    comment: string;
    signatures: {
      data: [
        {
          id: number;
          attributes: {
            firstname: string;
            lastname: string;
            status: string;
            comment: string;
            user: {
              data: {
                id: number;
                attributes: {
                  username: string;
                  email: string;
                  firstname: string;
                  lastname: string;
                  role: {
                    data: {
                      id: number;
                      attributes: {
                        name: string;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      ]
    }
  }
}
export interface ModuleWrapper extends StrapiResponse<Module>{}
export interface ModulesWrapper extends StrapiResponse<Module[]>{}
