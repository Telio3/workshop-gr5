import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './shared/guards/auth.guard';
import { DataUserGuard } from './shared/guards/data-user.guard';
import { ModuleComponent } from "./components/module/module.component";
import { RoleGuard } from './shared/guards/role.guard';
import {SignPageComponent} from "./components/signPage/signPage.component";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: DashboardComponent,
        canActivate: [DataUserGuard, AuthGuard],
    },
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'modules/:id',
        component: ModuleComponent,
        canActivate: [DataUserGuard, AuthGuard, RoleGuard],
    },
    {
      path: 'login/:data',
      component: LoginComponent,
    },
    {
      path: 'sign/:data',
      component: SignPageComponent,
      canActivate: [DataUserGuard, AuthGuard],
    },
    {
        path: '**',
        pathMatch: 'full',
        redirectTo: '',
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule { }
