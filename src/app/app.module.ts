import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { TokenInterceptor } from './shared/tools/token-interceptor';
import { QRCodeModule } from 'angularx-qrcode';

import { AppComponent } from './app.component';
import { SignatureComponent } from './shared/components/signature/signature.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { CalendarComponent } from './components/dashboard/calendar/calendar.component';
import { CurrentModuleComponent } from './components/dashboard/current-module/current-module.component';
import {SignPageComponent} from "./components/signPage/signPage.component";

import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { RippleModule } from 'primeng/ripple';
import { MenubarModule } from 'primeng/menubar';
import { DividerModule } from 'primeng/divider';
import { SelectButtonModule } from 'primeng/selectbutton';
import { TableModule } from 'primeng/table';
import { ModuleComponent } from "./components/module/module.component";
import { ToolbarModule } from "primeng/toolbar";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { PanelModule } from 'primeng/panel';
import { KnobModule } from 'primeng/knob';
import { ChipModule } from 'primeng/chip';
import { DialogModule } from "primeng/dialog";
import { ToastModule } from 'primeng/toast';
import { OnlineStatusModule } from 'ngx-online-status'

import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';

const PRIMENG = [
  ButtonModule,
  InputTextModule,
  RippleModule,
  MenubarModule,
  DividerModule,
  SelectButtonModule,
  TableModule,
  ToolbarModule,
  ConfirmDialogModule,
  NoopAnimationsModule,
  PanelModule,
  KnobModule,
  ChipModule,
  DialogModule,
  ToastModule,
];

FullCalendarModule.registerPlugins([
  dayGridPlugin,
  timeGridPlugin,
  interactionPlugin,
  listPlugin
]);

@NgModule({
  declarations: [
    AppComponent,
    SignatureComponent,
    LoginComponent,
    DashboardComponent,
    HeaderComponent,
    CalendarComponent,
    CurrentModuleComponent,
    ModuleComponent,
    SignPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    FullCalendarModule,
    QRCodeModule,
    OnlineStatusModule,
    ...PRIMENG,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
