export const environment = {
  production: true,
  urlApi: 'https://workshop-strapi.herokuapp.com/api/',
  urlApp: 'https://workshop-gr5.netlify.app/'
};
